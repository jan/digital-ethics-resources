include Makefile.d/defaults.mk

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

all: ## Run unit tests and build the program (DEFAULT)
all: dist
.PHONY: all

# TODO: Implement this in web-extension/Makefile
test:
	$(error "Not implemented yet")
.PHONY: test

markdown := $(shell find content/ -iname '*.md')
images-source := $(shell find content -iname '*.png' -or -iname '*.jpeg' -or -iname '*.jpg' -or -iname '*.webp')
videos-source := $(shell find content -iname '*.webm')

html := $(patsubst content/%.md,dist/%.html,$(markdown))
pngs := $(patsubst content/%.png,dist/%.png,$(images-source))
jpegs := $(patsubst content/%.jpeg,dist/%.jpeg,$(images-source))
videos := $(patsubst content/%.webm,dist/%.webm,$(videos-source))

dist: ## Content to the dist/ directory.
dist: $(html)
dist: $(pngs)
dist: $(jpegs)
dist: $(videos)
dist:
	echo $^
	touch $@

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install


dist/%.html: content/%.md content/header.html
	mkdir --parents $(@D)
	pandoc $< \
		--output=$@ \
		--include-in-header=content/header.html \
		--standalone

dist/%.png: content/%.png
	mkdir --parents $(@D)
	cp $< $@

dist/%.jpeg: content/%.jpeg
	mkdir --parents $(@D)
	cp $< $@

dist/%.webm: content/%.webm
	mkdir --parents $(@D)
	cp $< $@

### DEVELOPMENT

develop: ## Watch, rebuild and serve the content
develop:
	@
	if test $$(tmux display-message -p "#{session_name}") = "digital-ethics-development"
	then
		echo "You are are already in the development session."
		echo ""
		echo "If you are stuck, try to close this terminal, open a new one and run this command again."
		exit 1
	fi

	if tmux has-session -t "digital-ethics-development"
	then
		tmux kill-session -t "digital-ethics-development"
	fi

	tmux new-session -d -s "digital-ethics-development"

	tmux set-window-option -t "digital-ethics-development" remain-on-exit

	tmux split-window -t "digital-ethics-development" \
			-c "$(CURDIR)/"
	tmux send-keys -t "digital-ethics-development" \
			'make watch' Enter

	tmux split-window -t "digital-ethics-development" \
			-c "$(CURDIR)/"
	tmux send-keys -t "digital-ethics-development" \
			'make serve' Enter

	tmux select-layout -t "digital-ethics-development:" main-vertical
	tmux switch-client -t "digital-ethics-development:0.0"
.PHONY: develop

watch:
	watch make dist

serve: ## Serve the built program
serve: dist
serve:
	miniserve --index=index.html dist/
.PHONY: serve

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

serve-result: ## Serve the program built using Nix
serve-result: result
	miniserve --index=index.html result
.PHONY: serve-result
