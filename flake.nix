{
  description = "Catalogue of resources about the digital ethics";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.gnumake
            pkgs.pandoc
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "digital-ethics";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "digital-ethics-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
            pkgs.miniserve
            pkgs.ffmpeg
          ];
        };

      }
    );
}
